﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessHibirnation
{
    public class ProcessService
    {
        public enum ProcessType
            {
                cmd,
                pw //powershell
            };

        public void ProcessStart(ProcessType type, string command, bool waitForExit, bool threadSleep, int timesSleep = 0)
        {
            if (threadSleep) Thread.Sleep(timesSleep);
            Process process = Process.Start(new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                FileName = type == ProcessType.cmd ? @"cmd" : @"C:\Windows\SysWOW64\WindowsPowerShell\v1.0\powershell.exe",
                Arguments = type == ProcessType.cmd ? $"/c {command}" : $"/command {command}",


            });
            if (waitForExit)
            {
                process.WaitForExit();
            }

        }
    }
}
